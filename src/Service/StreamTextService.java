package src.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class StreamTextService implements ITextService{


    @Override
    public Map<String, Long> numbersOfRepetitions(String text) {
        return getWords(text).stream().collect(Collectors.groupingBy(Function.identity(), counting()));
    }

    @Override
    public Set<String> uniqueWords(String text) {
        return getWords(text).stream().collect(Collectors.toSet());
    }

    @Override
    public List<String> getWords(String text) {
        return Arrays.asList(text.split("[^а-яА-Яa-zA-Z]+"));

    }

    @Override
    public List<String> sortedList(String text) {
        TextComparator comparator = new TextComparator();
        return uniqueWords(text).stream().sorted(comparator.ALPHABETICAL_ORDER).collect(Collectors.toList());
    }

    @Override
    public List<String> sortedListV2(String text) {
        return uniqueWords(text).stream().sorted().collect(Collectors.toList());
    }


}
