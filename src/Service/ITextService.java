package src.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ITextService {
    Map<String, Long> numbersOfRepetitions(String text);
    Set<String> uniqueWords (String text);
    List<String> getWords (String text);
    List<String> sortedList (String text);
    List<String> sortedListV2 (String text);
}
