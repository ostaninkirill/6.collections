package src.Service;

import java.util.*;

public class TextService implements ITextService {

    @Override
    public Map<String, Long> numbersOfRepetitions(String text) {
        Map<String, Long> result = new HashMap<>();
        List<String> list = getWords(text);
        for (String word : list) {
            if (!result.containsKey(word)) {
                result.put(word, (long) Collections.frequency(list, word));
            }
        }

        return result;
    }

    @Override
    public Set<String> uniqueWords(String text) {
        return new HashSet<>(getWords(text));
    }


    public List<String> getWords (String text) {
        return Arrays.asList(text.split("[^а-яА-Яa-zA-Z]+"));
    }

    @Override
    public List<String> sortedList(String text) {
        List<String> result = new ArrayList<>(uniqueWords(text));
        TextComparator comparator = new TextComparator();
        result.sort(comparator.ALPHABETICAL_ORDER);
        return result;
    }

    @Override
    public List<String> sortedListV2(String text) {
        List<String> result = new ArrayList<>(uniqueWords(text));
        Collections.sort(result);
        return result;
    }

}
